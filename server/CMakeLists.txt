project(FormFactorServer VERSION ${FF_VERSION})

find_package(Qt5 ${REQUIRED_QT_VERSION} REQUIRED NO_MODULE COMPONENTS Core DBus)

add_subdirectory(src)

configure_file(org.cask.FormFactor.service.in
    ${CMAKE_CURRENT_BINARY_DIR}/org.cask.FormFactor.service)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/org.cask.FormFactor.service
    DESTINATION ${KDE_INSTALL_DBUSSERVICEDIR})

install(TARGETS ${PROJECT_NAME} ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
