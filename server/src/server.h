#pragma once

#include <QObject>

class Server : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint preferredMode READ preferredMode WRITE setPreferredMode NOTIFY preferredModeChanged)
    Q_PROPERTY(uint bestMode READ bestMode NOTIFY bestModeChanged)
    Q_PROPERTY(uint defaultMode READ defaultMode CONSTANT)

    Q_PROPERTY(bool hasKeyboard READ hasKeyboard WRITE setHasKeyboard NOTIFY hasKeyboardChanged)
    Q_PROPERTY(bool hasTouchscreen READ hasTouchscreen WRITE setHasTouchscreen NOTIFY hasTouchscreenChanged)
    Q_PROPERTY(bool hasMouse READ hasMouse WRITE setHasMouse NOTIFY hasMouseChanged)

    uint m_preferredMode;

    uint m_bestMode;

    uint m_defaultMode;

    bool m_hasKeyboard;

    bool m_hasTouchscreen;

    bool m_hasMouse;

public:
    explicit Server(QObject *parent = nullptr);

    uint preferredMode() const;

    uint bestMode() const;

    uint defaultMode() const;

    bool hasKeyboard() const;

    bool hasTouchscreen() const;

    bool hasMouse() const;

public slots:
    void setPreferredMode(uint preferredMode);

    void setBestMode(uint bestMode);

    void setDefaultMode(uint defaultMode);

    void setHasKeyboard(bool hasKeyboard);

    void setHasTouchscreen(bool hasTouchscreen);

    void setHasMouse(bool hasMouse);

signals:

    void preferredModeChanged(uint preferredMode);
    void bestModeChanged(uint bestMode);
    void defaultModeChanged(uint defaultMode);
    void hasKeyboardChanged(bool hasKeyboard);
    void hasTouchscreenChanged(bool hasTouchscreen);
    void hasMouseChanged(bool hasMouse);
};

