#include "server.h"
#include "formfactoradaptor.h"
#include <QDBusInterface>

Server::Server(QObject *parent) : QObject(parent)
{
    qDebug( " INIT BACKGORUND MODULE");
        new FormFactorAdaptor(this);
        if(!QDBusConnection::sessionBus().registerObject(QStringLiteral("/FormFactor"), this))
        {
            qDebug() << "FAILED TO REGISTER BACKGROUND DBUS OBJECT";
            return;
        }
}

uint Server::preferredMode() const
{
    return m_preferredMode;
}

uint Server::bestMode() const
{
    return m_bestMode;
}

uint Server::defaultMode() const
{
    return m_defaultMode;
}

bool Server::hasKeyboard() const
{
    return m_hasKeyboard;
}

bool Server::hasTouchscreen() const
{
    return m_hasTouchscreen;
}

bool Server::hasMouse() const
{
    return m_hasMouse;
}

void Server::setPreferredMode(uint preferredMode)
{
    if (m_preferredMode == preferredMode)
        return;

    m_preferredMode = preferredMode;
    emit preferredModeChanged(m_preferredMode);
}

void Server::setBestMode(uint bestMode)
{
    if (m_bestMode == bestMode)
        return;

    m_bestMode = bestMode;
    emit bestModeChanged(m_bestMode);
}

void Server::setDefaultMode(uint defaultMode)
{
    if (m_defaultMode == defaultMode)
        return;

    m_defaultMode = defaultMode;
    emit defaultModeChanged(m_defaultMode);
}

void Server::setHasKeyboard(bool hasKeyboard)
{
    if (m_hasKeyboard == hasKeyboard)
        return;

    m_hasKeyboard = hasKeyboard;
    emit hasKeyboardChanged(m_hasKeyboard);
}

void Server::setHasTouchscreen(bool hasTouchscreen)
{
    if (m_hasTouchscreen == hasTouchscreen)
        return;

    m_hasTouchscreen = hasTouchscreen;
    emit hasTouchscreenChanged(m_hasTouchscreen);
}

void Server::setHasMouse(bool hasMouse)
{
    if (m_hasMouse == hasMouse)
        return;

    m_hasMouse = hasMouse;
    emit hasMouseChanged(m_hasMouse);
}
