#include "serverapp.h"
#include "server.h"
#include <QDBusConnectionInterface>
#include <QDebug>

ServerApp::ServerApp(int &argc, char **argv) : QCoreApplication(argc, argv)
{

}

ServerApp::~ServerApp()
{
    qDeleteAll(m_modules);
}


bool ServerApp::init()
{
    QDBusConnectionInterface *iface = QDBusConnection::sessionBus().interface();

    if(iface->isServiceRegistered("org.cask.FormFactor"))
    {
        qWarning() << "FromFactor Service is already registered";
        return false;
    }

    auto registration = iface->registerService(QStringLiteral("org.cask.FormFactor"),
                                               QDBusConnectionInterface::ReplaceExistingService,
                                               QDBusConnectionInterface::DontAllowReplacement);

    if (!registration.isValid())
    {
        qWarning("2 Failed to register D-Bus service \"%s\" on session bus: \"%s\"",
                 qPrintable("org.cask.FormFactor"),
                 qPrintable(registration.error().message()));

        return false;
    }

    m_modules << new Server();
//    m_modules << new Theme(this);
//    m_modules << new Screen(this);
    return true;
}
