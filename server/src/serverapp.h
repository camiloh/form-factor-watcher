#pragma once

#include <QCoreApplication>
#include <QObject>

class ServerApp : public QCoreApplication
{
    Q_OBJECT
public:
    explicit ServerApp(int &argc, char **argv);
    ~ServerApp();
    bool init();

private:
    QVector<QObject*> m_modules;
};

